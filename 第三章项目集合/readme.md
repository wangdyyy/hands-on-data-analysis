## 第三章

本节用到了两个模型，
- 逻辑回归模型 <br>
简单易理解，模型的可解释性非常好；
- 随机森林分类模型<br>
不容易过拟合，效果比逻辑回归模型好。