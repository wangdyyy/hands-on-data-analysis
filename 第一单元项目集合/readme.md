## 第一小节 载入数据
`pd.read_csv('*.csv',chunksize=100)#分块读取数据`<br>
判断是否为空<br>
`df.isnull()`
`df.to_csv()#保存文件`

## 第二节 pandas 基础
- 1<br>
Series 一维数据,表示一列<br>
DateFrame 二维数据，表示一个表格<br><br>

- 2<br>
读取数据<br>
`pd.read_csv('/train.csv')`<br>

查看每一列<br>
`df.columns`<br>

查看指定列<br>
`df['*']`<br>
`df.*`<br>

删除某一列<br>
`del df['*']`<br>
`df.drop(name,axis,inplace=False) #name是行或者列的名称，axis是轴，0代表垂直方向(行），1代表水平方向(列)。inplace为False不改变原数据，True改变原数据。默认是False`

筛选内容<br>

`df[df["Age"]<10].head`
& 且   | 或
`df[['col_name','col_name']][df['col_name1']>20|df['col_name2']<40]#显示多列，多个条件`

重置索引<br>
`df.reset_index(drop=True)`

显示指定行、列<br>
```
df.loc[[100],['Pclass','Sex']]         #使用的是标签    行标签和列标签；
df.loc[[100,105,108],['Pclass','Sex']]  #
df.iloc[[100,105,108],[2,3,4]]         # 使用的是序号
```

## 第三小节 排序和简单分析

- 排序 <br>

```
df.sort_values(by=[col_name,*,*],ascending=True) #列标签排序
df.sort_index(axis=1,ascending=True)#列索引排序


df['col_name'].describe()#列标签统计
```