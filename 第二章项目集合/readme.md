## 第二章数据清洗
- 第一节<br>
__删除空项、重复项__ <br>
```
# describe()返回的是Series[1]   取数
df['客舱'].describe()[0]
df['年龄'].describe()[0]
df['登船港口'].describe()[0]

df.isnull().sum()#统计表格为空的信息

#删除数据项为空的数据
df.dropna(subset=['年龄']) # 指定非空的标签
```

__离散化处理__<br>
```
pd.cut(df['Age'],5,labels= [1,2,3,4,5])  #平均分成5份
pd.cut(df['Age'],[0,5,15,30,50,80],labels=[1,2,3,4,5]) #按指定区间划分
pd.qcut(df['Age'],[0,0.1,0.3,0.5,0.7,0.9],labels=[1,2,3,4,5])  #按百分比划分


get_dummies(df[feat],prefix=feat) #one hot encode 实现离散型特征的一个函数。
```

- 第二、三小节<br>

concat   将两个DF纵向连接<br>
join通过索引或者指定的列连接两个DataFrame。<br> 
append 在调用者的末尾追加其他行，返回一个新对象。<br>

merge 函数能够进行高效的合并操作<br>

stack()即“堆叠”，作用是将列旋转到行 <br>
unstack()即stack()的反操作，将行旋转到列<br>


__分组__<br>
任何分组(groupby)操作都涉及原始对象的以下操作之一。1.分割对象  2.应用一个函数  3.结合的结果<br>

聚合 - 计算汇总统计<br>
转换 - 执行一些特定于组的操作<br>
过滤 - 在某些情况下丢弃数据<br>

agg()使用指定轴上的一个或多个操作进行聚合。<br>


- 第四小节<br>

plt.title('survived_count') #标题
plt.ylabel('count')#  x,y轴
plt.xlabel('count')
柱状图：plot.bar()
热力图：.map()
